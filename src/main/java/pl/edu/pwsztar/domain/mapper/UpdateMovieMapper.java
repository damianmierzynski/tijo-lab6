package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.UpdateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class UpdateMovieMapper {

    public Movie updateMovie(Movie movie, UpdateMovieDto updateMovieDto){
        movie.setYear(updateMovieDto.getYear());
        movie.setImage(updateMovieDto.getImage());
        movie.setTitle(updateMovieDto.getTitle());
        movie.setVideoId(updateMovieDto.getVideoId());

        return movie;
    }
}
